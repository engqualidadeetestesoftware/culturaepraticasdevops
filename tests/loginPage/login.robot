*** Settings ***
Resource        ../../resources/PO/login.resource
Resource        ../../resources/suite_teardown.resource

Test Setup        Open web browser
Suite Teardown    Close Browser    ALL

*** Test Cases ***
TC01: Verificando texto de username obrigatório
    login.Verificando se o login e senha são obrigatórios

TC02: Verificando texto de password obrigatório
    login.Verificando se a senha é obrigatória

TC03: Verificando o bloqueio de entrada informando username e password incorretos
    login.Verificando se é bloqueada a entrada do sistema com login e senha erradas

Tc04: Verificando a entrada no sistema informando o username e password corretamente
    login.Validando a entrada do sistema com login e senha corretos